import React, {Component} from 'react';
import Comment from './Comment'


class Post extends Component {


// Initialize a state using a constructor() method for our Post to set a initial state. It should create a state value called body. Set it to the body of your hard-coded post.
  constructor(props) {
    // pass in props above and below to have access to them in your constructor
    super(props)

    this.state = {
      body: this.props.body
    }
  }


  handleClick = (event) => {
    console.log("the button was clicked")

    const userInput = prompt("type some shit in")
    this.edit(userInput)
  }


  edit = (newBody) => {
    this.setState({
      body: newBody
    })
  }

  render() {

    const comments = this.props.comments.map((comment, i) => {
      return(
        <Comment body={comment} key={i} />
      )
    })

    return(
      <div>
        <h1>{this.props.title}</h1>
        <div>by <small>{this.props.author}</small></div>
        <p>{this.state.body}</p>

        <h3>the comments section you shouldn't read</h3>
        
        {comments}
      
        <button onClick={this.handleClick}>Hey</button>
      </div>

    );

  }

}

export default Post