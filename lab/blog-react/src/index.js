import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Post from './Post';
import registerServiceWorker from './registerServiceWorker';

ReactDOM.render(
  <Post 
    title={"Star Trek DS9"} 
    author={"Rick Berman and Jeri Ryan"} 
    body={"Capt Sisko becomes a prophet (like a God) in the Bajoran religion "} 
    comments={["coooool", "i like space", "the show is too dark i have to turn up my breighnes sall the way to eevn sort of see it"]} 
  />, 
  document.getElementById('root')
);
registerServiceWorker();
