
### Discussion of the different parts of a React app

* notice the module webpack

* webpack gives us our development environment
  * like hot reloading (runs the server for you, restarts it on change)

* this line is what runs the server for us that serves the app: 
```
"start": "react-scripts start" 
```

* in `src` it has JS files and CSS files.  

* notice: 1 CSS file per JS file

* in public you see html page -- this is the one page that contains the `#root` div.

* that `#root` div is where the whole application will go (because we're using React to write SPAs (Single Page Applications).

* notice that `src/index.js` is injecting the `App` component into that `#root` div

* you can pretty much ignore registerServiceWorker

* Now look at `src/index.js`

* `import` statement, this is like the ES6 version of `require()` with `module.exports`

* notice we are importing ReactDOM, and calling its `render()` method and saying what we wanna render and where (`#root`)

* if you start the app (`npm start`), you see Starting development server (thanks, webpack)

* browser will open, or you can go to localhost:3000

* inspect and see the app embedded in the `#root` div


* look at `App.js`:

  * when `render()` gets called, everything that is inside the `return()` gets converted into html.  that is what gets inserted into `#root`

  * all the html is included in one div

  * it looks, especially now, very much like html, but it's technically a template that's being compiled and injected. 


* COMPONENTS: 

  * think of components like a function -- should do 1 thing and do it very well

  * you can render components inside of other components

  * 2 types

    * **smart** components, or **container** component, the container components actually have the data in them.  **they have state.  data is stored in state**.  this is generally their purpose

    * **presentational** components -- they only take data and render (a piece of) the UI


* Data flow in react: 

  * ONE-WAY data flow. top-down.

  * Data flows from top of application down into whatever components

  * Generally, you have a smart component at the top where data is managed, and the container passes it down into presentational components as necessary

  * in picture, `<Network/>` container components that contains multiple `<Line/>` presentational components


* Refactoring

  * very common in React

  * as app evolves, you find you need to break big components into smaller ones, make smart components dumb, make dumb components smart, etc.


* STATE AND PROPS

 - let's make our hello world a little dynamic

 - **props** allows you to pass data from one component down into another component

 - you can pass props to **any** component

 - looks like an html attribute

 ```
 // we are passing a name prop(erty) down to the Hello component
// the property will have the value of "Freddie"
```
 - you can pass **any** datatype you want objects, arrays, strings, numbers

 - they will be available in in the `this.props` object of the component you're passing them to

 - note you can't `console.log()` inside the return.  so do it before.


* STATE

  - state is data pertaining to the state of your app

  - react's job is actually ultimatel to render state

  - to create state you MUST have a `constructor()` in that component's class
  - AND YOU MUST call `super()` inside that constructor.

  - so we added state.

  - every time we update state, the `render()` method gets called

  - THE WAY (the one true and only way) to update state is with `this.setState()`. pass in an object with the new state for example.
  ```
  this.setState({
    counter: this.state.counter + 1
  })
  ```
  - so when we click button, 
    - it calls the `handleClick()` method we attached to the button
    - handle click calls `this.setState()` and passes in an object with the key and the new value
    - `this.setState()` AUTOMAGICALLY calls `render()` so that the new value is reflected on the Page


  - Virtual DOM - a representation of the DOM behind the scenes of the real DOM. constains states of all the divs and components. etc
  - react is smart enough to inspect this anytime something is changed and automatically **only update what is necessary** 
  - updating the real DOM is expensive, and this helps us much more efficiently update only what needs updating.


Use arrow functions

  - arrow functions in React automatically bind to this for you
  - if you use non-arrow functions, it will not do this

  - you often see methods declared the regular way, but then in constructor, you have to 
  - this.myMethod = this.myMethod.bind(this)

  - in React, arrow functions will do this for you automatically 


Review:

  - every time you update state, the data flows down from that component anywhere you've passed it down as a prop

  - when you call this.setState(), the data flows from that component all the way down