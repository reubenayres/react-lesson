//importing an instance of the Component class and React itself from
// the 'react' module
import React, { Component } from 'react';

class Hello extends Component {

  constructor() {
    // call super to initialize the Component constructor
    // this is what allows us to use `this` inside of the constructor
    super()

    // declare initial values for state stuff here
    this.state = {
      counter: 0
    }

  }

  handleClick = (e) => {
    // e is the event (a click, in this case) from the button click
    // so you can still use e.currentTarget, etc.

    // this.setState updates state AND AUTOMATICALLY 
    // (re-)calls the render() function
    this.setState({
      counter: this.state.counter + 1
    })
  }

  // render method gets called automatically
  // and should return one piece of UI
  render() {
    // you MUST call return in a render()
    // note you can only return one thing
    // (although there can be multiple things 
    // embedded within that one thing)
    // note: do not put comments inside a return

    // you can check the value of this.props like this
    // note: no console.logs inside return
    // note: it is very helpful to say what method and which component
    console.log(this.props, ' this.props in render() in Hello')

    // a console.log so we can see state changing each time render is called
    // (remember: calling this.setState updates state, 
    // which means that render() will run again)

    return(
      <div>
        <h1>Hello {this.props.name}</h1>
        <p>My number is {this.props.number}</p>
        <img src={this.props.pic} alt="Lieutenant Commander Data" width="300"/> 
        <p>The current count is {this.state.counter}</p>
        <button onClick={this.handleClick}> Update Counter </button>
      </div>
    )
  }
}

export default Hello;
