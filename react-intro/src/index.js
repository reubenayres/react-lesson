import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Hello from './Hello';
import registerServiceWorker from './registerServiceWorker';

// we are passing a name prop(erty) down to the Hello component
// the property will have the value of "Freddie"
ReactDOM.render(<Hello name={"Freddie"} number={3125551212} pic={'https://ladygeekgirl.files.wordpress.com/2017/10/data-suaveness-engaged.jpg'} />, document.getElementById('root'));
registerServiceWorker();
